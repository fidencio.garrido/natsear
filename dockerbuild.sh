#!/usr/bin/env bash
go build --ldflags '-extldflags "-static"' natsear.go Routes.go
docker rmi natsear:latest
go build natsear.go Routes.go
rm ./build/natsear
mv natsear ./build/natsear
cd build && docker build -t=natsear:latest .
rm build/natsear
echo Creating a container
docker run -it --rm --name ear natsear:latest -nats-server nats://192.168.1.25:4222