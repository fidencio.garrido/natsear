# Changelog

The official document for this project.

## [0.2.0]
### Added
- UI

## [0.1.0]
### Added
- Subscribe to a topic API
- Get the list of listeners for topics API
- Message counter per topic API
- Subscriptions stats (summarized view of NATS subz) API
- List of all received messages with timestamp and topic included