module.exports = (grunt)=>{
	grunt.initConfig({
		copy:{
			external:{
				files: [
					{expand: true, src: ["./**/*.*"], dest: "../public/external/bootstrap", cwd: "bower_components/bootstrap/dist"},
					{expand: true, src: ["./**/*.*"], dest: "../public/external/vue", cwd: "bower_components/vue/dist"},
					{expand: true, src: ["./**/*.*"], dest: "../public/external/font-awesome/css", cwd: "bower_components/font-awesome/css"},
					{expand: true, src: ["./**/*.*"], dest: "../public/external/font-awesome/fonts", cwd: "bower_components/font-awesome/fonts"},
					{expand: true, src: ["./**/*.*"], dest: "../public/external/jquery", cwd: "bower_components/jquery/dist"}

				]
			}
		},
		pug: {
			compile: {
				src: ["**/*.pug"],
				dest: "../public/",
				expand: true,
				cwd: "views/impl",
				rename(dest, src){
					return dest + src.replace(".pug",".html");
				}
			},
		},
		watch: {
			recompile:{
				files: ["views/**/*.pug", "../public/js/**/*.*"],
				tasks: ["pug"],
				options: {
					livereload: true
				}
			}
		}
	});

	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks("grunt-contrib-pug");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.registerTask("default", ["copy","watch"]);
};