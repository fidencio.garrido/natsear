# NATS ear
NATS topic listener

## Usage

Example starting the application in port 80
```sh
$ natsear -p 80
```

### Command line options

**-help** Displays help

**-p** Application port, by default ```8080```

**-nats-server** URL for the NATS Server, by default ```nats://localhost:4222```


## API

Subscribing to a topic
```
POST /topic/{topicName}
```

Get the list of topic listeners (the actual expression used during subscription)
```
GET /topic
```

Get the count of messages received per topic
```
GET /stats/topic
```

Get the stats for the subscriptions
```
GET /stats/subs
```

Get the list of messages for the session
```
GET /message
```