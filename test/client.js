"use strict";

var request = require("superagent"),
	expect = require("chai").expect;

let nats = require("nats").connect(),
	topics = ["ignoretopic", "listentopic1", "listentopic2", "twice", "twice"],
	msg = "Hello world",
	natsear = "http://localhost:8080";

describe("Subscribing", ()=>{
	before(()=>{

	});

	it("should reply 200 to the wildcard subscription (*)", (done)=>{
		request
			.post(`${natsear}/topic/*`)
			.end( (err,res)=>{
				expect(res.status).to.equal(200);
				done();
			} )
	});

});

describe("Getting list of listeners", ()=>{

	it("should return (*) when querying the topics it is configured to listen to", (done)=>{
		request.get(`${natsear}/topic`)
			.end( (err,res)=>{
				expect(res.status).to.equal(200);
				expect(err).to.equal(null);
				let body = JSON.parse( res.text );
				expect(body.length).to.equal(1);
				expect(body[0]).to.equal("*");
				done();
			});
	});

	it(`should publish successfully to ${topics.length} topics`, ()=>{
		topics.forEach( (t)=> {
			nats.publish(t, `Hello ${t}`);
		});
	});
});


describe("Getting stats for topic", ()=>{

	it("should read topic stats", (done)=>{
		request.get(`${natsear}/stats/topic`)
			.end( (err, res)=>{
				expect(res.status).to.equal(200);
				expect(err).to.equal(null);
				let body = JSON.parse(res.text);
				expect(body).to.be.a("object");
				expect(body.twice).to.be.a("number");
				done();
			});
	});

});
