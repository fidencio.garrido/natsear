package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/nats-io/nats"
	"log"
	"net/http"
	"strings"
	"time"
)

var topics = map[string]int{}
var listeners = make([]string, 0)
var messageQueue = make([]*Message, 0)

type Connection struct {
	Cid                int      `json:"cid"`
	Ip                 string   `json:"ip"`
	In_msgs            int      `json:"in_msgs"`
	Out_msgs           int      `json:"out_msgs"`
	Subscriptions      int      `json:"subscriptions"`
	Subscriptions_list []string `json:"subscriptions_list"`
}

type Connections struct {
	Num_connections int          `json:"num_connections"`
	Connections     []Connection `json:"connections"`
}

type Message struct {
	TimeStamp time.Time
	Topic     string
	Message   string
}

func indexOf(slice []string, searchTerm string) int {
	for p, v := range slice {
		if v == searchTerm {
			return p
		}
	}
	return -1
}

func routeAddTopicListener(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		vars := mux.Vars(r)
		topic := vars["topicName"]
		ndx := indexOf(listeners, topic)
		if ndx >= 0 {
			log.Print("I cannot add again that listener " + topic)
			fmt.Fprintln(w, "Topic already registered")
		} else {
			log.Print("Listening to topic: ", topic)
			listeners = append(listeners, topic)
			natsconnection.Subscribe(topic, topicHanlder)
			topics[topic] = 0
			fmt.Fprintln(w, "ok")
		}
	}
}

func getAllListeners(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		mapped, _ := json.Marshal(listeners)
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w, string(mapped))
	}
}

func getCountByTopic(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		mapped, _ := json.Marshal(topics)
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w, string(mapped))
	}
}

func topicHanlder(m *nats.Msg) {
	var count = topics[m.Subject]
	topics[m.Subject] = (count + 1)
	log.Print("Subject:", m.Subject, "\tCount:", count)
	fmt.Printf("Received message: %s for topic %s\n", string(m.Data), string(m.Subject))
	message := new(Message)
	message.TimeStamp = time.Now()
	message.Message = string(m.Data)
	message.Topic = string(m.Subject)
	messageQueue = append(messageQueue, message)
}

func getSubscriptions(w http.ResponseWriter, r *http.Request) {
	serverInfo := strings.Split(*nserver, ":")
	endpoint := "http:" + serverInfo[1] + ":8222/connz"
	resp, err := http.Get(endpoint)
	if err != nil {
		log.Printf("Error getting NATS stats")
		fmt.Fprintln(w, "NATS Error")
	} else {
		var connections Connections
		json.NewDecoder(resp.Body).Decode(&connections)
		json.NewEncoder(w).Encode(connections)
	}
}

func getAllMessages(w http.ResponseWriter, r *http.Request) {
	mapped, _ := json.Marshal(messageQueue)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(mapped))
}

/**
ToDo
- Add an endpoint to return current active topics
- Add an endpoint to get the list of current subscribers
*/
