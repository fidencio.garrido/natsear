package main

import (
	"flag"
	"github.com/gorilla/mux"
	"github.com/nats-io/nats"
	"log"
	"net/http"
	"time"
)

const VERSION = "0.2.0"

var port = flag.String("p", "8080", "Application port")
var help = flag.Bool("help", false, "Displays help information")
var nserver = flag.String("nats-server", "nats://localhost:4222", "NATS Server host")
var natsconnection nats.Conn

func connect() bool {
	connected := false
	log.Printf("Connecting to " + *nserver)
	nc, _e := nats.Connect(*nserver)
	if _e != nil {
		log.Printf("NATS Error: " + _e.Error())
	}
	if nc != nil {
		log.Printf("Connected to NATS")
		natsconnection = *nc
		connected = true
	}
	return connected
}

func startWebService() {
	mux := mux.NewRouter()
	publicDir := http.Dir("./public")
	mux.HandleFunc("/topic/{topicName}", routeAddTopicListener)
	mux.HandleFunc("/topic", getAllListeners)
	mux.HandleFunc("/stats/topic", getCountByTopic)
	mux.HandleFunc("/stats/subs", getSubscriptions)
	mux.HandleFunc("/message", getAllMessages)
	mux.PathPrefix("/").Handler(http.FileServer(publicDir))
	server := &http.Server{
		Addr:        "0.0.0.0:" + *port,
		Handler:     mux,
		ReadTimeout: 10 * time.Second,
	}
	log.Printf("Service started in port " + *port)
	server.ListenAndServe()
}

func main() {
	flag.Parse()
	if *help == true {
		println("NATS ear", VERSION)
		flag.PrintDefaults()
		println("By Fido")
	} else {
		println("NATS ear", VERSION)
		if connect() {
			startWebService()
		} else {
			log.Printf("Closing service. Please come back when NATS is available")
		}
	}
}
