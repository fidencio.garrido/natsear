const app = new Vue({
	el: "#app",
	data:{
		currentTopic: "",
		togglelisten: false,
		topics: [],
		new_topic: ""
	},
	methods: {
		natssend(topic,message){
			const _url = `/message/${topic}`;
			return new Promise( (resolve, reject)=>{
				$.ajax({
					url: _url,
					method: "post",
					data: message,
					success(data){
						resolve(data);
					},
					error(err){
						reject(err);
					}
				});
			});
		},
		natssubscribe(topic){
			const _url = `/topic/${topic}`;
			return new Promise( (resolve, reject)=>{
				$.ajax({
					url: _url,
					method: "post",
					success(data){
						resolve(topic);
					},
					error(err){
						reject(err);
					}
				});
			});
		},
		publish(topic){

		},
		subscribe(){
			let self = this;
			this.natssubscribe(this.new_topic).then((tname)=>{
				self.topics.push(tname);
				self.new_topic = "";
			}).catch( (err)=>{
				alert("Cannot subscribe to topic, more detail available in the console");
				console.log(err);
			});
		}
	}
});